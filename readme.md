## Code test

To run this project you have to fellow bellow steps :

- First clone project
- Then Create database in your host and update .env file
- Then register user 
- Enjoy.

### Activity

- Create mail button open email form
- SMTP info open SMTP info form

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
