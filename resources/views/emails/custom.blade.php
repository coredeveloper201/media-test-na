@component('mail::message')
Hello {{ $info['name'] }},<br>
{!! $info['message'] !!}

Thanks,<br>
{{ $name }}
@endcomponent
