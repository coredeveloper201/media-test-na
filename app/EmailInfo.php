<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class EmailInfo extends Model
{
    protected $fillable = [
        'user_id', 'name', 'email', 'subject', 'message', 'status'
    ];

    public function user() {
        return $this->belongsTo(\App\User::class);
    }

    public function getCreatedAtAttribute() {
        return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }
}
