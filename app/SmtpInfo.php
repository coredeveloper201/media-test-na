<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmtpInfo extends Model
{
    protected $fillable = [
        'user_id', 'name', 'password', 'server', 'driver',
         'encryption', 'port'
    ];

    public function user() {
        return $this->belongsTo(\App\User::class);
    }
}
