<?php

namespace App\Http\Controllers;

use App\SmtpInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SmtpController extends Controller
{
    public function index()
    {
        return response()->json(
            Auth::user()->smtp
        );
    }

    public function store(Request $request)
    {
        return SmtpInfo::updateOrCreate(
            ['user_id' => $request->user()->id],
            $request->all()
        );
    }
}
