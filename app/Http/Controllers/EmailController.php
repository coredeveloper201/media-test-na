<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Mail\UserEmail;
use App\EmailInfo;

class EmailController extends Controller
{

    public function index()
    {
        return response()->json(
            EmailInfo::orderBy('created_at','desc')->get()
        );
    }

    public function send(Request $request)
    {
        $email = EmailInfo::create($request->all() + [
            'user_id' => Auth::user()->id
        ]);
        \Mail::to($request->email)
            ->send(new UserEmail($request->all()));
        $email->update(['status' => 1]);
        return response()->json(true);
    }
}
