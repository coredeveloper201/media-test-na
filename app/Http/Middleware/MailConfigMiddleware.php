<?php

namespace App\Http\Middleware;

use Closure;

class MailConfigMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $mail = $request->user()->smtp;
        $config = array(
            'driver' => $mail->driver,
            'host' => $mail->server,
            'port' => $mail->port,
            'from' => array(
                'address' => $mail->name,
                'name' => $request->user()->name
            ),
            'encryption' => $mail->encryption,
            'username' => $mail->name,
            'password' => $mail->password
        );

        \Config::set('mail', $config);
        $app = \App::getInstance();
        $app->register('Illuminate\Mail\MailServiceProvider');

        return $next($request);
    }
}
